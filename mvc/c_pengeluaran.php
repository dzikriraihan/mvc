<?php

include_once("m_pengeluaran.php");

class c_pengeluaran
{
    public $model;

    public function __construct()
    {
        $this->model = new m_pengeluaran();
    }

    public function invoke()
    {
        $catatan = $this->model->getAll();
        include 'v_pengeluaran.php';
    }

    public function insert($id_pengeluaran, $nama_pengeluaran, $jumlah, $tanggal)
    {
        $this->model->setCatatan($id_pengeluaran, $nama_pengeluaran, $jumlah, $tanggal);
        header('Location: ' . "index.php");
    }

    public function deleteAction($num)
    {
        $this->model->delete($num);
        header('Location: ' . "index.php");
    }

    public function updateAction($num, $nama, $jumlah, $tanggal)
    {
        $this->model->update($num, $nama, $jumlah, $tanggal);
        header('Location: ' . "index.php");
        
    }
}
