<?php

require "./connection.php";

class m_pengeluaran
{
    private $database;
    protected $tablename = "catatan";

    public function __construct()
    {
        $this->database = new connection();
        $this->database = $this->database->mysqli;
    }

    public function setCatatan($id_pengeluaran, $nama_pengeluaran, $jumlah, $tanggal)
    {
        $id_pengeluaran = $this->database->real_escape_string($id_pengeluaran);
        $nama_pengeluaran = $this->database->real_escape_string($nama_pengeluaran);
        $jumlah = $this->database->real_escape_string($jumlah);
        $tanggal = $this->database->real_escape_string($tanggal);

        $query = "INSERT INTO $this->tablename (id_pengeluaran, nama_pengeluaran, jumlah, tanggal) VALUES ('$id_pengeluaran', '$nama_pengeluaran', '$jumlah', '$tanggal')";
        return $this->database->query($query);
    }

    public function getAll()
    {
        $result = $this->database->query("SELECT * FROM $this->tablename");
        $programs = $result->fetch_all(MYSQLI_ASSOC);
        return $programs;
    }

    public function delete($id)
    {
        $id = $this->database->real_escape_string($id);
        return $this->database->query("DELETE FROM $this->tablename WHERE id_pengeluaran = '$id'");
    }

    public function update($id, $nama, $jumlah, $tanggal)
    {
        $id = $this->database->real_escape_string($id);
        $nama = $this->database->real_escape_string($nama);
        $jumlah = $this->database->real_escape_string($jumlah);
        $tanggal = $this->database->real_escape_string($tanggal);

        return $this->database->query("UPDATE $this->tablename SET nama_pengeluaran = '$nama', jumlah = '$jumlah', tanggal = '$tanggal' WHERE id_pengeluaran = '$id'");
    }
}
