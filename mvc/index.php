<?php
session_start();
include_once("c_pengeluaran.php");

$controller = new c_pengeluaran();

    if (isset($_GET['add'])) {
        $id_pengeluaran = $_POST['id_pengeluaran'];
        $nama_pengeluaran = $_POST['nama_pengeluaran'];
        $jumlah = $_POST['jumlah'];
        $tanggal = $_POST['tanggal'];
        $controller->insert($id_pengeluaran, $nama_pengeluaran, $jumlah, $tanggal);
    } elseif (isset($_GET['edit'])) {
        $num = $_GET['edit'];
        $nama = $_POST['nama_pengeluaran'];
        $jumlah = $_POST['jumlah'];
        $tanggal = $_POST['tanggal'];
        $controller->updateAction($num, $nama, $jumlah, $tanggal);
    } elseif (isset($_GET['delete'])) {
        $nomor = $_GET['delete'];
        $controller->deleteAction($nomor);
    } else {
        $controller->invoke();
    }
?>
