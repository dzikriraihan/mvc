<?php
$id_pengeluaran = $_GET['edit'] ?? '';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Catatan</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .container {
            max-width: 400px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.1);
            margin-top: 50px;
        }

        .form-group {
            margin-bottom: 20px;
        }

        .form-group label {
            font-weight: bold;
            display: block;
            margin-bottom: 5px;
        }

        .form-group input {
            width: calc(100% - 22px);
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .form-group button {
            background-color: #0078DF;
            color: white;
            border: none;
            border-radius: 5px;
            padding: 10px 20px;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <div class="container">
        <form action="index.php?edit=<?php echo $id_pengeluaran; ?>" method="post">
            <h1>Edit Catatan Pengeluaran</h1>
            <div class="form-group">
                <label for="id_pengeluaran">ID Pengeluaran:</label>
                <input type="text" id="id_pengeluaran" name="id_pengeluaran" value="<?php echo $id_pengeluaran; ?>" required readonly />
            </div>
            <div class="form-group">
                <label for="nama_pengeluaran">Nama Pengeluaran:</label>
                <input type="text" id="nama_pengeluaran" name="nama_pengeluaran" required />
            </div>
            <div class="form-group">
                <label for="jumlah">Jumlah:</label>
                <input type="text" id="jumlah" name="jumlah" required />
            </div>
            <div class="form-group">
                <label for="tanggal">Tanggal:</label>
                <input type="text" id="tanggal" name="tanggal" required />
            </div>
            <div class="form-group">
                <button type="submit">Simpan Perubahan</button>
            </div>
        </form>
    </div>
</body>

</html>
