<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="<?= BASE_URL.'index.php?r=home/insertBarang'?>" class="btn btn-primary">Tambah Barang</a>
    <br><br>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Nama Barang</th>
                <th>QTY</th>
            </tr>
        </thead>
        <?php foreach($data as $item) : ?>
        <tr scope="row">
            <td><?php echo $item['id']; ?></td>
            <td><?php echo $item['nama']; ?></td>
            <td><span class="badge badge-<?= $item['qty']>50 ? 'success' : 'danger'?>"><?= $item['qty'] ?></span></td>
            <td><a href="<?= BASE_URL.'index.php?r=home/updateBarang/'.$item['id']?>" class="badge">Update</a>
                <a href="<?= BASE_URL.'index.php?r=home/deleteBarang/'.$item['id']?>" class="badge" onclick="return confirm('apakah anda yakin untuk menghpaus data?')">Delete</a>
            </td>
        </tr>
        <?php endforeach ?>
    </table>
</body>
</html>