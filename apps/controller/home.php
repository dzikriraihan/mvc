<?php 
    class home extends controller {

        private $dt;
        private $df;

        public function __construct() {
            // echo "anda berada pada controller home \n";
            $this -> dt = $this -> loadmodel("barang"); //object
            $this -> df = $this -> loadmodel("daftarBarang");
        }

        public function index() {
            echo "anda memanggil action index \n";
        }

        public function home($data1, $data2) {
            echo "anda memanggil action home dengan data1 = $data1 dan data2 = $data2\n";
        }

        public function lihatData($id) {
            $data =  $this -> df -> getDataById($id);

            $this -> loadview('template/header', ['title' => 'Detail Barang']);
            $this -> loadview('home/detailBarang', $data);
            $this -> loadview('template/footer');
        }

        public function listBarang() {
            $data = $this->df->getDataAll();

            $this -> loadview('template/header', ['title' => 'List Barang']);
            $this -> loadview('home/listBarang', $data);
            $this -> loadview('template/footer');
            // foreach ($this->df->getDataAll() as $key => $item) {
            //     echo $item['id']. " ". $item['nama']. " ". $item['qty'];
            //     echo "<br/>";
            // }
        }

        public function insertBarang() {
            if (!empty($_POST)) {
                if ($this -> df -> tambahBarang($_POST)) {
                    header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                    exit;
                }
            }

            $this -> loadview('template/header', ['title' => 'Insert Barang']);
            $this -> loadview('home/insert');
            $this -> loadview('template/footer');
        }

        public function updateBarang($id) {
            $data = $this -> df -> getDataById($id);

            if (!empty($_POST)) {
                if ($this -> df -> updateBarang($_POST)) {
                    header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                    exit;
                }
            }
            
            $this -> loadview('template/header', ['title' => 'Update Barang']);
            $this -> loadview('home/update', $data);
            $this -> loadview('template/footer');
        }
        public function deleteBarang($id) {
            $data = $this -> df -> getDataById($id);

            if ($this -> df -> hapusBarang($id)) {
                header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                exit;
            }
        }

        
    }
?>