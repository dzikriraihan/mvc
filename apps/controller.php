<?php 
    class controller {
        // handle model : 
        // 1. cek apakah file model ada
        // 2. jika ada, maka require kelas model
        // 3. instansiasi pada kelas model

        public function loadmodel ($model) {
            if (file_exists('apps/model/'.$model.'.php')) {
                require_once('apps/model/'.$model.'.php');
                $model = new $model;
            }
            return $model;
        }

        public function loadview ($view, $data=null) {
            if (file_exists('apps/view/'.$view.'.php')) {
                require_once('apps/view/'.$view.'.php');
            }
        }
    }
?>